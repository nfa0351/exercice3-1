package com.codebind;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;
import java.text.CharacterIterator;
import java.text.StringCharacterIterator;

import static java.awt.Color.ORANGE;
import static java.awt.Color.WHITE;

public class BadgeSizeCellRenderer extends DefaultTableCellRenderer {

    private Color originalBackground;
    private Color originalForeground;


    /**
     * constructeur par défaut sans paramètre + bind des get background / foreground
     */
    public BadgeSizeCellRenderer() {
        this.originalBackground = this.getBackground();
        this.originalForeground = this.getForeground();
    }

    /**
     * retourne le rendu de la cellule
     */
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

        Long taille = (Long) value; //cast en long la taille du badge
        setText(humanReadableByteCountBin(taille)); //appel de la fonction HumanReadable avec taille en param
        if (taille>10240) { // si taille > 10ko
            setBackground(ORANGE);
        } else {
            setBackground(WHITE);
        }
        return this; //couleur de fond par ligne
    }

    /**
     * permet de retourner la valeur en octet ou kilo octet
     * @param bytes
     * @return
     */
    private String humanReadableByteCountBin(long bytes) {
        long absB = bytes == Long.MIN_VALUE ? Long.MAX_VALUE : Math.abs(bytes);
        if (absB < 1024) {
            return bytes + " o";
        }
        long value = absB;
        CharacterIterator ci = new StringCharacterIterator("KMGTPE");
        for (int i = 40; i >= 0 && absB > 0xfffccccccccccccL >> i; i -= 10) {
            value >>= 10;
            ci.next();
        }
        value *= Long.signum(bytes);
        return String.format("%.1f %co", value / 1024.0, ci.current());
    }
}
