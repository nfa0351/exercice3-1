package com.codebind;

import javax.swing.*;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import fr.cnam.foad.nfa035.badges.wallet.dao.*;
import fr.cnam.foad.nfa035.badges.wallet.dao.impl.DirectAccessBadgeWalletDAOImpl;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;


public class BadgeWalletGUI {

    private static final String RESOURCES_PATH = "exercice3-1/badges-wallet/src/test/resources/";
    private JButton button1;
    private JPanel panel1;
    private JTable table1;

    private Icon displayLogo;

    public BadgeWalletGUI() {
        button1.addActionListener(new ActionListener() {
            /**
             * Invoked when an action occurs.
             *
             * @param e the event to be processed
             */
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null, "Bien joué!");
            }
        });

        DirectAccessBadgeWalletDAO dao;

        try {
            dao = new DirectAccessBadgeWalletDAOImpl(RESOURCES_PATH + "db_wallet_indexed.csv");
            Set<DigitalBadge> metaSet = dao.getWalletMetadata();
            List<DigitalBadge> tableList = new ArrayList<>();
            tableList.addAll(metaSet);
            //TableModel tableModel = fr.cnam.foad.nfa035.badges.gui.TableModelCreator.createTableModel(DigitalBadge.class, tableList);

            BadgesModel tableModel = new BadgesModel(tableList);

            table1.setModel(tableModel);

            table1.getColumnModel().getColumn(4).setCellRenderer(new BadgeSizeCellRenderer());
            table1.setDefaultRenderer(Object.class, new DefaultBadgeCellRenderer());
            table1.setDefaultRenderer(Number.class, new DefaultBadgeCellRenderer());
            table1.setDefaultRenderer(Date.class, new DefaultBadgeCellRenderer());

            /**
             * activation du tri
             */
            table1.setAutoCreateRowSorter(true);
            table1.getRowSorter().toggleSortOrder(0);

        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("My Badge Wallet"); //instanciation JFrame
        BadgeWalletGUI gui = new BadgeWalletGUI(); //instanciation BadgeWalletGUI
        JFrame.setDefaultLookAndFeelDecorated(true); // plus joli ?
        frame.setContentPane(gui.panel1); //assignation du panel1 de l'instance de BadgeWalletGUI
        frame.setIconImage(Toolkit.getDefaultToolkit().getImage("idf.gif"));
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false); // empêche le redimentionnement
        frame.setVisible(true);
        frame.pack();
    }

}
