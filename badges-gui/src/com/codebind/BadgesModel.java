package com.codebind;

import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;

import javax.swing.table.AbstractTableModel;
import java.util.Date;
import java.util.List;


public class BadgesModel extends AbstractTableModel {

    /**
     * lien d'aggrégation avec badge
     */
    private List<DigitalBadge> badges;
    private String[] entetes = {"ID", "Code Série", "Début", "Fin", "Taille (octets)"};

    /**
     * le numéro de série random
     */
    private static final Long serialVersionUID = 8684686168131L;

    /**
     * constructeur avec 3 paramètres
     * @param badges
     */
    public BadgesModel(List<DigitalBadge> badges) {
        super();
        this.badges = badges;
    }

    //public BadgesModel(List<DigitalBadge> badges) {}

    /**
     * récupère liste des badges
     * @return badges
     */
    public List<DigitalBadge> getBadges() {
        return badges;
    }

    /**
     * récupère la class de colonne
     * @param columnIndex the column being queried
     * @return
     */
    @Override
    public Class<?> getColumnClass(int columnIndex) {
        //return super.getColumnClass(columnIndex);
        switch (columnIndex) {
            case 0: //badgeID as Integer
                return Integer.class;

            case 1: //serial as String
                return String.class;

            case 2: //date début as Date
                return Date.class;

            case 3: //date fin as Date
                return Date.class;

            case 4: //taille image as Long
                return Long.class;

            default: //si aucun autre
                return Object.class;
        }
    }

    /**
     * récupère le nom de la colonne
     */
    @Override
    public String getColumnName(int columnIndex) {
        //return super.getColumnName(column); // autogénération
        return entetes[columnIndex];
    }

    /**
     * Returns the number of rows in the model. A
     * <code>JTable</code> uses this method to determine how many rows it
     * should display.  This method should be quick, as it
     * is called frequently during rendering.
     *
     * @return the number of rows in the model
     * @see #getColumnCount
     */
    @Override
    public int getRowCount() {
        return badges.size();
    }

    /**
     * Returns the number of columns in the model. A
     * <code>JTable</code> uses this method to determine how many columns it
     * should create and display by default.
     *
     * @return the number of columns in the model
     * @see #getRowCount
     */
    @Override
    public int getColumnCount() {
        return entetes.length;
    }

    /**
     * Returns the value for the cell at <code>columnIndex</code> and
     * <code>rowIndex</code>.
     *
     * @param rowIndex    the row whose value is to be queried
     * @param columnIndex the column whose value is to be queried
     * @return the value Object at the specified cell
     */
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        //return null;
        switch (columnIndex) {
            case 0: // ID
                return badges.get(rowIndex).getMetadata().getBadgeId();

            case 1: // Serial
                return badges.get(rowIndex).getSerial();

            case 2: // date début
                return badges.get(rowIndex).getBegin();

            case 3: // date fin
                return badges.get(rowIndex).getEnd();

            case 4: // taille image
                return badges.get(rowIndex).getMetadata().getImageSize();

            default: // si paramètre incorrect
                throw new IllegalArgumentException();
        }
    }

}


