package com.codebind;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;
import java.util.Date;

import static java.awt.Color.*;

/**
 * classe pour définir l'habillage graphique du programme
 */
public class DefaultBadgeCellRenderer extends DefaultTableCellRenderer {

   protected Color originForeground = null;

   /**
    * constructeur par défaut sans paramètre + bind du get Foreground
    */
   public DefaultBadgeCellRenderer() {
      this.originForeground = this.getForeground();
   }

   /**
    * retourne le rendu de la cellule
    * @param table  the <code>JTable</code>
    * @param value  the value to assign to the cell at
    *                  <code>[row, column]</code>
    * @param isSelected true if cell is selected
    * @param hasFocus true if cell has focus
    * @param row  the row of the cell to render
    * @param column the column of the cell to render
    * @return
    */
   @Override
   public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
      super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

      /**
       * si la date de fin < date courante
       */
      if (((Date) table.getModel().getValueAt(row, 3)).after(new Date())) {
         setForeground(RED);
      } else {
         setForeground(BLACK);
      }

      return this; // couleur texte par ligne
   }
}
