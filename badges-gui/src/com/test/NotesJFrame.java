package com.test;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
//import java.io.Serial;

public class NotesJFrame extends JFrame {

    private static final long serialVersionUID = 3928008548751894521L;

    /**
     * aggrégation avec objet NotesModel
     */
    private NotesModele modele;
    /**
     * aggrégation avec objet JTable
     */
    private JTable table;

    public NotesJFrame() throws NullPointerException, IllegalArgumentException, IllegalComponentStateException {

        super();
        setTitle("Notes des élèves");
        setPreferredSize(new Dimension(500, 400));
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        modele = new NotesModele(); // appel du constructeur par défaut
        table = new JTable(modele); // constructs a JTable that is initialized with dm as the data model
        table.setAutoCreateRowSorter(true); // pour afficher option tri
        table.setDefaultRenderer(Sexe.class, new SexeCellRenderer()); // renderer pour toutes les cellules de type Sexe
        table.getColumnModel().getColumn(4).setCellRenderer(new NotesCellRenderer()); // renderer fond de cellule

        getContentPane().add(new JScrollPane(table)); // returns the contentPane object for this frame

        pack(); //force la prise en compte des paramètres dessus
    }
}
