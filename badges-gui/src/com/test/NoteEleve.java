package com.test;

public class NoteEleve {
    private Eleve eleve; //objet de la classe Eleve
    private Double note;

    public NoteEleve(Eleve eleve, Double note) {
        this.eleve = eleve;
        this.note = note;
    }

    public Eleve getEleve() {
        return eleve;
    }

    public Double getNote() {
        return note;
    }

    public void setEleve(Eleve eleve) {
        this.eleve = eleve;
    }

    public void setNote(Double note) {
        this.note = note;
    }
}
