package com.test;

import java.util.ArrayList;
import java.util.List;

import static com.test.Sexe.FEMME;
import static com.test.Sexe.HOMME;

public class NoteService {


    /**
     * Crée l'instance unique de l'objet > Pattern Singleton
     */
    private static NoteService instance;

    /**
     * Collection ordonnée des notes élèves nommée notes
     */
    private List<NoteEleve> notes;

    /**
     * Constructeur avec 1 param
     */
    private NoteService(List<NoteEleve> notes) {
        this.notes = notes;
    }

    /**
     * constructeur par défaut
     */
    public NoteService() {}

    /**
     * Getter de Instance > contrôle qu'il n'existe qu'une fois !
     * @return
     */
    public static synchronized NoteService getInstance() {
        if(instance == null) {
            instance = new NoteService();
        }
        return instance;
    }

    /**
     * Stub des données (simulation)
     */
    private void chargerDepuisBaseDonnees() {

        if (notes != null) {
            return;
        }

        notes = new ArrayList<NoteEleve>();

        addEleveEtNote("Durand", "Marie", 3, FEMME, 5.0);
        addEleveEtNote("Alessi", "Julie", 3, FEMME, 8.0);
        addEleveEtNote("Michelet", "Jean", 3, HOMME, 0.0);
        addEleveEtNote("Dupont", "Pierre", 3, HOMME, 2.0);
        addEleveEtNote("Timberot", "Martin", 3, HOMME, 4.0);
        addEleveEtNote("Gravatas", "Paul", 3, HOMME, 5.0);
        addEleveEtNote("Dominici", "Adrien", 3, HOMME, 10.0);
        addEleveEtNote("Dupont", "Albert", 3, HOMME, 14.0);
        addEleveEtNote("Obino", "Alex", 3, HOMME, 9.0);
        addEleveEtNote("Formi", "Alexandre", 3, HOMME, 13.0);
        addEleveEtNote("Livradu", "Alice", 3, FEMME, 14.0);
    }

    public synchronized List<NoteEleve> findLastNotes() {
        chargerDepuisBaseDonnees();
        return notes;
    }

    private void addEleveEtNote(String nom, String prenom, Integer annee, Sexe sexe, Double note) {
        final Eleve eleve = new Eleve(nom, prenom, annee, sexe);

        final NoteEleve noteEleve = new NoteEleve(eleve, note); // appel des 2 constructeurs eleve et note
        notes.add(noteEleve);
    }







}
