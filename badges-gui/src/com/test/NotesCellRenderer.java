package com.test;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;

public class NotesCellRenderer extends DefaultTableCellRenderer {

    private static final long serialVersionUID = 5813843843184842L;

    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

        Double note = (Double) value;
        setText(note.toString());
        if (note < 10) {
            setBackground(Color.RED);
        } else {
            setBackground(Color.GREEN);
        }
        return this;
    }
}
