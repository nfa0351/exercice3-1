package com.test;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;

import static com.test.Sexe.FEMME;
import static java.awt.Color.BLUE;
import static java.awt.Color.PINK;

/**
 * for rendering (displaying) individual cells in a JTable.
 */
public class SexeCellRenderer extends DefaultTableCellRenderer {

    private static final long serialVersionUID = -13848421821223L;

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

        Sexe sexe = (Sexe) value;
        setText(sexe.getLabel());

        if (sexe == FEMME) {
            setForeground(PINK);
        } else {
            setForeground(BLUE);
        }
        return this;
    }

}
