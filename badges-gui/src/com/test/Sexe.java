package com.test;

public enum Sexe {
    HOMME("Garçon"),
    FEMME("Fille");

    private final String label;

    /**
     * constructeur de l'énum > ne peut être public ?
     * @param label
     */
    Sexe(String label) {
        this.label = label;
    }

    /**
     * Getter de label
     * @return
     */
    public String getLabel() {
        return label;
    }


}

