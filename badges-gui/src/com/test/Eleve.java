package com.test;

public class Eleve {
    private String nom;
    private String prenom;
    private Integer annee;
    private Sexe sexe; //objet de la classe Enum Sexe

    public Eleve(String nom, String prenom, Integer annee, Sexe sexe) {
        this.nom = nom;
        this.prenom = prenom;
        this.annee = annee;
        this.sexe = sexe;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public void setAnnee(Integer annee) {
        this.annee = annee;
    }

    public void setSexe(Sexe sexe) {
        this.sexe = sexe;
    }

    public String getPrenom() {
        return prenom;
    }

    public Integer getAnnee() {
        return annee;
    }

    public Sexe getSexe() {
        return sexe;
    }
}
