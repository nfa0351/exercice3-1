package com.test;

import javax.swing.table.AbstractTableModel;
import java.util.List;

/**
 * Table model MINIMAL
 */
public class NotesModele extends AbstractTableModel { //AbstractTableModel pour implementation Row / Column / Data

    private final String[] entetes = {"Nom", "Prénom", "Année", "Sexe", "Note"};

    private static final long serialVersionUID = -112842428822878L;

    /**
     * Composition avec objet NoteService
     */
    private NoteService noteService;

    private List<NoteEleve> notes;

    /**
     * constructeur avec 1 param
     */
    public NotesModele() { // ancien param : List<NoteEleve> notes
        super();
        noteService = NoteService.getInstance(); //recupère instance singleton de noteService ?
        notes = noteService.findLastNotes(); // invoque chargerDepuisBaseDeDonnées
    }

    /**
     * Returns the number of rows in the model. A
     * <code>JTable</code> uses this method to determine how many rows it
     * should display.  This method should be quick, as it
     * is called frequently during rendering.
     *
     * @return the number of rows in the model
     * @see #getColumnCount
     */
    @Override
    public int getRowCount() {
        return notes.size();
    }

    /**
     * Returns the number of columns in the model. A
     * <code>JTable</code> uses this method to determine how many columns it
     * should create and display by default.
     *
     * @return the number of columns in the model
     * @see #getRowCount
     */
    @Override
    public int getColumnCount() {
        return entetes.length; // 5 ici
    }

    @Override
    public String getColumnName(int columnIndex) {
        return entetes[columnIndex]; // num de la colonne
    }

    /**
     * Returns the value for the cell at <code>columnIndex</code> and
     * <code>rowIndex</code>.
     *
     * @param rowIndex    the row whose value is to be queried
     * @param columnIndex the column whose value is to be queried
     * @return the value Object at the specified cell
     */
    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {

        switch (columnIndex) {

            case 0:
                return notes.get(rowIndex).getEleve().getNom();

            case 1:
                return  notes.get(rowIndex).getEleve().getPrenom();

            case 2:
                return notes.get(rowIndex).getEleve().getAnnee();

            case 3:
                return notes.get(rowIndex).getEleve().getSexe();

            case 4:
                return notes.get(rowIndex).getNote();

            default:
                throw new IllegalArgumentException();
        }

    }

    /**
     * permet de distinguer le type de données associées à chaque cellule
     * @param columnIndex  the column being queried
     * @return
     */
//@Override
    public Class<?> getColumnClass(int columnIndex) {

        switch (columnIndex) {
            case 0:
            case 1:
                return String.class;

            case 3:
                return Sexe.class;

            case 2:
                return Integer.class;

            case  4:
                return Double.class;

            default:
                return Object.class;
        }
    }

    public List<NoteEleve> getNotes() {
        return notes;
    }


}
